const express = require("express");
const app = express();
const cors = require('cors');
const port = 3000;
const bodyParser = require("body-parser");
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
app.use(cors());

//Creating a mongoose connection and schema of the database
const mongoose = require("mongoose");
mongoose.connect(
  "mongodb://amruthteja7:cbit1234@ds259001.mlab.com:59001/futrieve",
  { useNewUrlParser: true }
);

//Creating Pet Collection
const petSchema = new mongoose.Schema(
  {
    PetName: String,
    OwnerName: String,
    weight: Number,
    diet: String,
    behavior: String
  },
  {
    timestamps: true
  }
);
const pet = mongoose.model("pet", petSchema);

//Creating Behavior Collection
const behaviorSchema = new mongoose.Schema(
  {
    behavior: String
  }
);

const behavior = mongoose.model("behavior", behaviorSchema);


app.get("/", (req, res) => {
  res.send("Enter pet data");
});

//Getting the pet data
app.get("/pets", (req, res) => {
  pet.find({}).then(data => {
    res.json(data);
  });
});

//Adding a new pet data
app.post("/addPet", (req, res) => {
  var petData = new pet(req.body);
  petData.save()
    .then(item => {
      res.send(item);
    })
    .catch(err => {
      res.status(400).send("Unable to save to database");
    });
});

//Getting pet data by id
app.get("/pets/:pet_id", (req, res) => {
  pet.findById(req.params.pet_id)
    .then(pet => {
      res.json(pet);
    })
    .catch(err => {
      res.status(404).send(err);
    });
});

//Updating the pet data by id
app.put("/pets/:pet_id", (req, res) => {
  var update = { $set: {
    weight: req.body.weight
  }}
  pet.updateOne(update)
    .then(pet => {
      res.json(pet);
    });
});

//Removing the pet data by id
app.delete("/pets/:pet_id", (req, res) => {
  pet.findOneAndRemove({}).then(pet => {
    res.json("Deleted pet data");
  });
});

// Getting Behavior Data
app.get("/behavior", (req, res) => {
  behavior.find({}).then(data => {
    res.json(data);
  });
});

//Adding Pet Behavior Data
app.post("/addBehavior", (req, res) => {
  var Data = new behavior(req.body);
  Data.save()
    .then(item => {
      res.send(item);
    })
    .catch(err => {
      res.status(400).send("Unable to save to database");
    });
});

app.listen(process.env.PORT || 3000, () => {
  console.log("Server listening on port " + port);
});
